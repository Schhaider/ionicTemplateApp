import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Page01 } from '../page01/page01';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  StringToPage:string;
  constructor(public navCtrl: NavController) {

  }
    goToPage(event,StringTOPage) {
      // That's right, we're pushing to ourselves!
      this.navCtrl.push(Page01, { stringtoPage:StringTOPage});
  }
    /*goToPage(event, item) {
      // That's right, we're pushing to ourselves!
      this.navCtrl.push(Page01, { });
  }*/
}
