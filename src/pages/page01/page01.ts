import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the Page01 page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-page01',
  templateUrl: 'page01.html',
})
export class Page01 {
  private stringFromPage:String;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.stringFromPage=this.navParams.get('stringtoPage');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Page01');
  }

}
